﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using NUnit.Framework;
using Orion.Elements.Cli;
using UnityEngine;
using UnityEngine.TestTools;

namespace Origin.Elements.Cli.Tests
{
    [TestFixture]
    public class TrashTests
    {
        [Test]
        public void CanonicalFileNameSimplePasses()
        {
            //BuildProjectCommand cmd = new BuildProjectCommand();
            //cmd.GetCanonicalFileName();

            var name = GetCanonicalProjectName("员工!");
            //var name = GetCanonicalProjectName("asd!");
            TestContext.WriteLine(name);
        }

        [Test]
        public void ListHandlers()
        {
            var enm = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(p => p.DefinedTypes)
                .Where(p => p.IsClass && !p.IsAbstract && typeof(ICliOptionHandler).IsAssignableFrom(p))
                .Select(p => Activator.CreateInstance(p) as ICliOptionHandler)
                .OrderBy(p => p.Order);

            foreach (var handler in enm)
                TestContext.WriteLine(handler.OptionName);
        }

        private string GetCanonicalProjectName(string projectName)
        {
            string name = Regex.Replace(projectName.Trim(), "[^.a-zA-Z0-9 -]", "");
            if (string.IsNullOrEmpty(name))
                throw new InvalidOperationException($"Failed to get canonical project name form string '{projectName}'");

            return name.ToLower().Replace(' ', '-');
        }
    }
}
