﻿namespace Orion.Elements.Cli
{
    public interface ICliCommand
    {
        string Tag { get; }
    }
}
