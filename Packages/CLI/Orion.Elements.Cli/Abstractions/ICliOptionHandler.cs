﻿namespace Orion.Elements.Cli
{
    public interface ICliOptionHandler
    {
        string OptionName { get; }

        int Order { get; }

        void Process(CliOption option);
    }
}
