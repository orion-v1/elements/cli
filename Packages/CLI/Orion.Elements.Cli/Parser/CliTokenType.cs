﻿namespace Orion.Elements.Cli
{
    public enum CliTokenType
    {
        Unknown = 0,
        Program,
        Option,
        Value
    }
}
