﻿namespace Orion.Elements.Cli
{
    public class CliReader
    {
        public readonly string[] Args;

        int currentTokenIndex = -1;


        public CliReader(string[] args)
        {
            Args = args;
        }


        private void StepBack()
        {
            if (currentTokenIndex > -1)
                currentTokenIndex--;
        }


        public CliToken GetNextToken()
        {
            if (currentTokenIndex >= Args.Length - 1)
                return null;

            currentTokenIndex++;
            string text = Args[currentTokenIndex].Trim();

            if (currentTokenIndex == 0)
            {
                return new CliToken(CliTokenType.Program, text);
            }

            if (text.Length > 1 && text.StartsWith("-"))
            {
                return new CliToken(CliTokenType.Option, text.TrimStart('-'));
            }

            return new CliToken(CliTokenType.Value, text);
        }


        public CliToken GetNextOption()
        {
            while (true)
            {
                CliToken token = GetNextToken();

                if (token == null)
                    return null;

                if (token.Type == CliTokenType.Option)
                    return token;
            }
        }


        public CliToken GetNextValue()
        {
            CliToken token = GetNextToken();

            if (token == null)
                return null;

            if (token.Type == CliTokenType.Value)
                return token;

            StepBack();
            return null;
        }
    }
}
