﻿using System.Collections.Generic;

namespace Orion.Elements.Cli
{
    public class CliOption
    {
        public CliToken OptionToken { get; }
        public CliToken[] ValueTokens { get; }

        internal CliOption(CliToken optionToken, CliReader reader)
        {
            OptionToken = optionToken;

            List<CliToken> values = new List<CliToken>();
            while (true)
            {
                CliToken token = reader.GetNextValue();
                if (token == null)
                    break;

                values.Add(token);
            }
            ValueTokens = values.ToArray();
        }

        public bool IsMatchedToOption(string optionName) => CliToken.Equals(OptionToken, optionName);

        public CliToken GetValueOrDefault(int index)
            => index >= 0 && index < ValueTokens.Length ? ValueTokens[index] : null;
    }
}
