﻿using System;
using System.Collections.Generic;

namespace Orion.Elements.Cli
{
    public class CliToken
    {
        public readonly CliTokenType Type;
        public readonly string Text;

        internal CliToken(CliTokenType tokenType, string text)
        {
            Type = tokenType;
            Text = text;
        }


        public string ToStringOrDefault(string defaultValue = default) => Text ?? defaultValue;

        public int ToIntOrDefault(int defaultValue = default) => int.TryParse(Text, out var value) ? value : defaultValue;

        public bool ToBooleanOrDefault(bool defaultValue = default) => bool.TryParse(Text, out var value) ? value : defaultValue;


        public bool Equals(string text, StringComparison stringComparison = StringComparison.OrdinalIgnoreCase)
        {
            return string.Equals(Text, text, stringComparison);
        }

        public static bool Equals(CliToken token, string text, StringComparison stringComparison = StringComparison.OrdinalIgnoreCase)
        {
            return token != null && string.Equals(token.Text, text, stringComparison);
        }
    }
}
