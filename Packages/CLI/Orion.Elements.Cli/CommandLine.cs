﻿using System;
using UnityEditor;
using UnityEngine;

namespace Orion.Elements.Cli
{
    public static class CommandLine
    {
        /// <summary>
        /// <para>
        /// !!! DO NOT USE IT DIRECTLY !!!
        /// </para>
        /// Makes the project build
        /// </summary>
        public static void BuildProject()
        {
            var cmd = new BuildProjectCommand();
            cmd.RegisterCustomOptionHandlers();
            cmd.AddCommandLineArguments();

            ExecuteCommand(cmd);
        }

        /// <summary>
        /// <para>
        /// !!! DO NOT USE IT DIRECTLY !!!
        /// </para>
        /// Modifies preprocessor directives
        /// </summary>
        public static void ModifyPreprocessorDirectives()
        {
            var cmd = new ModifyPreprocessorDirectivesCommand();
            cmd.AddCommandLineArguments();

            ExecuteCommand(cmd);
        }

        /// <summary>
        /// <para>
        /// !!! DO NOT USE IT DIRECTLY !!!
        /// </para>
        /// Does nothing but process custom options
        /// </summary>
        public static void Execute()
        {
            var cmd = new VoidCommand();
            cmd.RegisterCustomOptionHandlers();
            cmd.AddCommandLineArguments();

            ExecuteCommand(cmd);
        }

        private static void ExecuteCommand(CliCommand command)
        {
            int exitCode;
            try
            {
                command.Validate();
                exitCode = command.Execute();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                exitCode = 1;
            }

            EditorApplication.Exit(exitCode);
        }
    }
}
