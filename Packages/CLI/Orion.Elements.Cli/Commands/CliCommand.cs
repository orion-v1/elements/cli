﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Orion.Elements.Cli
{
    internal abstract class CliCommand : ICliCommand
    {
        private Dictionary<string, Action<CliOption>> _options = new Dictionary<string, Action<CliOption>>();


        public abstract string Tag { get; }

        public abstract void Validate();

        public abstract int Execute();


        protected void RegisterOptionHandler(string optionName, Action<CliOption> action)
        {
            optionName = optionName.ToLowerInvariant();

            Action<CliOption> handler;
            if (_options.TryGetValue(optionName, out handler))
                handler += action;
            else
                handler = action;

            _options[optionName] = handler;
        }

        public void RegisterCustomOptionHandlers()
        {
            var enm = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(p => p.DefinedTypes)
                .Where(p => p.IsClass && !p.IsAbstract && typeof(ICliOptionHandler).IsAssignableFrom(p))
                .Select(p => Activator.CreateInstance(p) as ICliOptionHandler)
                .OrderBy(p => p.Order);

            foreach (var handler in enm)
                RegisterOptionHandler(handler.OptionName, handler.Process);
        }

        public void AddCommandLineArguments()
        {
            AddCommandLineArguments(Environment.GetCommandLineArgs());
        }

        public void AddCommandLineArguments(string[] args)
        {
            if (args == null)
                return;

            CliReader reader = new CliReader(args);
            
            while (true)
            {
                CliToken token = reader.GetNextOption();
                if (token == null)
                    break;

                CliOption option = new CliOption(token, reader);
                if (_options.TryGetValue(token.Text.ToLowerInvariant(), out var action))
                    action(option);
            }
        }
    }
}
