﻿namespace Orion.Elements.Cli
{
    internal class VoidCommand : CliCommand
    {
        public override string Tag => "Void";

        public override int Execute() => 0;

        public override void Validate() { }
    }
}
