﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace Orion.Elements.Cli
{
    public enum BuildProjectExportMode
    {
        Project,
        Apk,
        Aab,
    }

    internal class BuildProjectCommand : UnityCommand
    {
        [Serializable]
        private class KeystoreCredentials
        {
#pragma warning disable 0649
            public string KeystorePassword;
            public string KeyAlias;
            public string KeyPassword;
#pragma warning restore 0649
        }

        public override string Tag => "BuildProject";

        public bool IsSimulated { get; set; }

        public string BuildsRootPath { get; private set; } = "Builds";
        public string ProjectName { get; private set; } = PlayerSettings.productName;
        public string ProjectVersion { get; private set; } = PlayerSettings.bundleVersion;
        public int? BuildNumber { get; private set; }
        public string BuildSuffix { get; private set; }
        public string BuildReportPath { get; private set; }
        public BuildProjectExportMode? ExportMode { get; private set; }
        public string KeyStorePath { get; private set; }
        private string KeyStorePassword { get; set; }
        public string KeyAlias { get; private set; }
        private string KeyPassword { get; set; }
        public string AppleDeveloperTeamId { get; private set; }
        public bool IsDevelopmentBuild { get; private set; }
        public bool ScriptDebuggingEnabled { get; private set; }
        public bool ProfilerEnabled { get; private set; }

        public BuildProjectCommand()
        {
            RegisterOptionHandler("simulate", HandleSimulate);
            RegisterOptionHandler("buildsRootPath", HandleBuildsRootPath);
            RegisterOptionHandler("projectName", HandleProjectName);
            RegisterOptionHandler("projectVersion", HandleProjectVersion);
            RegisterOptionHandler("buildNumber", HandleBuildNumber);
            RegisterOptionHandler("buildSuffix", HandleBuildSuffix);
            RegisterOptionHandler("buildReportPath", HandleBuildReportPath);
            RegisterOptionHandler("exportMode", HandleExportMode);
            RegisterOptionHandler("keystorePath", HandleKeystorePath);
            RegisterOptionHandler("keystorePassword", HandleKeystorePassword);
            RegisterOptionHandler("keystoreCredentialsFile", HandleKeystoreCredentialsFile);
            RegisterOptionHandler("keyAlias", HandleKeyAlias);
            RegisterOptionHandler("keyPassword", HandleKeyPassword);
            RegisterOptionHandler("appleTeamId", HandleAppleDeveloperTeamId);
            RegisterOptionHandler("developmentBuild", HandleDevelopmentBuild);
            RegisterOptionHandler("enableScriptDebugging", HandleEnableScriptDebugging);
            RegisterOptionHandler("enableProfiler", HandleEnableProfiler);
        }

        #region Parsing

        protected override void HandleBuildTarget(CliOption option)
        {
            base.HandleBuildTarget(option);

            if (ExportMode.HasValue)
                return;

            // set default values
            switch (BuildTarget)
            {
                case BuildTarget.Android: ExportMode = BuildProjectExportMode.Apk; break;
                default: ExportMode = BuildProjectExportMode.Project; break;
            }
        }

        private void HandleSimulate(CliOption option)
        {
            IsSimulated = true;
        }

        private void HandleBuildsRootPath(CliOption option)
        {
            CliToken value = option.GetValueOrDefault(0);
            if (value != null)
                BuildsRootPath = value.Text;
        }

        private void HandleProjectName(CliOption option)
        {
            CliToken value = option.GetValueOrDefault(0);
            if (value != null)
                ProjectName = value.Text;
        }

        private void HandleProjectVersion(CliOption option)
        {
            CliToken value = option.GetValueOrDefault(0);
            if (value != null)
                ProjectVersion = value.Text;
        }

        private void HandleBuildNumber(CliOption option)
        {
            CliToken value = option.GetValueOrDefault(0);
            if (value == null)
                return;

            int num = value.ToIntOrDefault(-1);
            if (num >= 0)
                BuildNumber = num;
        }

        private void HandleBuildSuffix(CliOption option)
        {
            CliToken value = option.GetValueOrDefault(0);
            if (value != null)
                BuildSuffix = value.Text;
        }

        private void HandleBuildReportPath(CliOption option)
        {
            CliToken value = option.GetValueOrDefault(0);
            if (value != null)
                BuildReportPath = value.Text;
        }

        private void HandleExportMode(CliOption option)
        {
            CliToken value = option.GetValueOrDefault(0);
            if (value == null)
            {
                Debug.LogWarning("Export mode is not specified.");
                return;
            }

            // parse mode
            BuildProjectExportMode? mode = null;
            if (value.Equals("project")) ExportMode = BuildProjectExportMode.Project;
            else if (value.Equals("apk")) ExportMode = BuildProjectExportMode.Apk;
            else if (value.Equals("aab")) ExportMode = BuildProjectExportMode.Aab;

            // check value
            switch (BuildTarget)
            {
                case BuildTarget.iOS:
                    if (mode != BuildProjectExportMode.Project)
                    {
                        Debug.LogWarning($"Build target {BuildTarget} only supports 'project' export modes.");
                        mode = null;
                    }
                    break;

                case BuildTarget.Android:
                    if (mode != BuildProjectExportMode.Project &&
                        mode != BuildProjectExportMode.Apk &&
                        mode != BuildProjectExportMode.Aab)
                    {
                        Debug.LogWarning($"Build target {BuildTarget} doesn't support export mode {mode}.");
                        mode = null;
                    }
                    break;

                default:
                    Debug.LogWarning($"Build target {BuildTarget} doesn't support any export modes.");
                    mode = null;
                    break;
            }

            // set new value
            if (mode.HasValue)
                ExportMode = mode;
        }

        private void HandleKeystorePath(CliOption option)
        {
            CliToken value = option.GetValueOrDefault(0);
            if (value != null)
                KeyStorePath = value.Text;
        }

        private void HandleKeystorePassword(CliOption option)
        {
            CliToken value = option.GetValueOrDefault(0);
            if (value != null)
                KeyStorePassword = value.Text;
        }

        private void HandleKeyAlias(CliOption option)
        {
            CliToken value = option.GetValueOrDefault(0);
            if (value != null)
                KeyAlias = value.Text;
        }

        private void HandleKeyPassword(CliOption option)
        {
            CliToken value = option.GetValueOrDefault(0);
            if (value != null)
                KeyPassword = value.Text;
        }

        private void HandleKeystoreCredentialsFile(CliOption option)
        {
            CliToken value = option.GetValueOrDefault(0);
            if (value == null)
                return;

            string json = File.ReadAllText(value.Text);
            var credentials = JsonUtility.FromJson<KeystoreCredentials>(json);

            if (!string.IsNullOrEmpty(credentials.KeystorePassword)) KeyStorePassword = credentials.KeystorePassword;
            if (!string.IsNullOrEmpty(credentials.KeyAlias)) KeyAlias = credentials.KeyAlias;
            if (!string.IsNullOrEmpty(credentials.KeyPassword)) KeyPassword = credentials.KeyPassword;
        }

        private void HandleAppleDeveloperTeamId(CliOption option)
        {
            CliToken value = option.GetValueOrDefault(0);
            if (value != null)
                AppleDeveloperTeamId = value.Text;
        }

        private void HandleDevelopmentBuild(CliOption option)
        {
            IsDevelopmentBuild = true;
        }

        private void HandleEnableScriptDebugging(CliOption option)
        {
            IsDevelopmentBuild = true;
            ScriptDebuggingEnabled = true;
        }

        private void HandleEnableProfiler(CliOption option)
        {
            IsDevelopmentBuild = true;
            ProfilerEnabled = true;
        }

        #endregion


        public string GetCanonicalProjectName()
        {
            string name = Regex.Replace(ProjectName.Trim(), "[^.a-zA-Z0-9 -]", "");
            if (string.IsNullOrEmpty(name))
                throw new InvalidOperationException($"Failed to get canonical project name form string '{ProjectName}'");

            return name.ToLower().Replace(' ', '-');
        }

        public string GetCanonicalProjectVersion()
        {
            string ver = ProjectVersion;

            if (BuildNumber.HasValue)
                ver = $"{ver}.{BuildNumber}";

            return ver;
        }

        public string GetCanonicalFileName()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(GetCanonicalProjectName());
            sb.Append('-');
            sb.Append(GetCanonicalProjectVersion());

            if (IsDevelopmentBuild)
                sb.Append("-dbg");

            if (!string.IsNullOrEmpty(BuildSuffix))
            {
                sb.Append('-');
                sb.Append(BuildSuffix);
            }

            return sb.ToString();
        }

        public string GetBuildOutputPath()
        {
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(BuildsRootPath))
            {
                sb.Append(BuildsRootPath);
                sb.Append('/');
            }

            sb.Append(GetCanonicalFileName());

            // android specific
            if (BuildTarget == BuildTarget.Android)
            {
                switch (ExportMode)
                {
                    case BuildProjectExportMode.Apk: sb.Append(".apk"); break;
                    case BuildProjectExportMode.Aab: sb.Append(".aab"); break;
                }
            }

            return sb.ToString();
        }

        public BuildPlayerOptions CreateBuildPlayerOptions()
        {
            BuildPlayerOptions options = new BuildPlayerOptions();
            options.target = BuildTarget;
            if (options.targetGroup == BuildTargetGroup.Unknown)
                options.targetGroup = BuildPipeline.GetBuildTargetGroup(BuildTarget);

            options.locationPathName = GetBuildOutputPath();
            options.options = /*BuildOptions.ShowBuiltPlayer |*/ BuildOptions.StrictMode;

            if (IsDevelopmentBuild) options.options |= BuildOptions.Development;
            if (ScriptDebuggingEnabled) options.options |= BuildOptions.AllowDebugging;
            if (ProfilerEnabled) options.options |= BuildOptions.ConnectWithProfiler;

            options.scenes = EditorBuildSettings.scenes
                .Where(p => p.enabled)
                .Select(p => p.path)
                .ToArray();

            return options;
        }

        private void Setup()
        {
            if (!string.IsNullOrEmpty(ProjectVersion))
                PlayerSettings.bundleVersion = ProjectVersion;

            // android settings
            if (BuildTarget == BuildTarget.Android)
            {
                if (BuildNumber.HasValue)
                    PlayerSettings.Android.bundleVersionCode = BuildNumber.Value;

                switch (ExportMode)
                {
                    case BuildProjectExportMode.Project:
                        EditorUserBuildSettings.exportAsGoogleAndroidProject = true;
                        break;

                    case BuildProjectExportMode.Apk:
                        EditorUserBuildSettings.exportAsGoogleAndroidProject = false;
                        EditorUserBuildSettings.buildAppBundle = false;
                        break;

                    case BuildProjectExportMode.Aab:
                        EditorUserBuildSettings.exportAsGoogleAndroidProject = false;
                        EditorUserBuildSettings.buildAppBundle = true;
                        break;
                }

                if (!string.IsNullOrEmpty(KeyStorePath))
                {
                    PlayerSettings.Android.keystoreName = KeyStorePath;
#if UNITY_2019_1_OR_NEWER
                    PlayerSettings.Android.useCustomKeystore = true;
#endif
                }

                if (!string.IsNullOrEmpty(KeyStorePassword)) PlayerSettings.Android.keystorePass = KeyStorePassword;
                if (!string.IsNullOrEmpty(KeyAlias)) PlayerSettings.Android.keyaliasName = KeyAlias;
                if (!string.IsNullOrEmpty(KeyPassword)) PlayerSettings.Android.keyaliasPass = KeyPassword;
            }

            // ios settings
            else if (BuildTarget == BuildTarget.iOS)
            {
                if (BuildNumber.HasValue)
                    PlayerSettings.iOS.buildNumber = BuildNumber.Value.ToString();

                if (!string.IsNullOrEmpty(AppleDeveloperTeamId))
                    PlayerSettings.iOS.appleDeveloperTeamID = AppleDeveloperTeamId;
            }
        }


        public override void Validate()
        {
            // check build target
            AssertBuildTargetDefined();
        }

        public override int Execute()
        {
            // setup
            Setup();

            // create build option
            var options = CreateBuildPlayerOptions();

            // build project
            bool success;
            if (IsSimulated)
            {
                Debug.Log($"Simulate build project. Output: {options.locationPathName}");
                success = true;
            }
            else
            {
                var report = BuildPipeline.BuildPlayer(options);
                success = report.summary.result == BuildResult.Succeeded && report.summary.totalErrors == 0;

                // report
                if (success && !string.IsNullOrEmpty(BuildReportPath))
                    WriteReport(BuildReportPath, options, report);
            }

            return success ? 0 : 1;
        }

        private void WriteReport(string path, BuildPlayerOptions options, BuildReport buildReport)
        {
            using (var writer = File.CreateText(path))
            {
                writer.Write('{');

                writer.Write($"\"buildOutputPath\": \"{options.locationPathName}\",");
                writer.Write($"\"platform\": \"{buildReport.summary.platform}\",");
                writer.Write($"\"platformGroup\": \"{buildReport.summary.platformGroup}\",");

                string applicationIdentifier = PlayerSettings.GetApplicationIdentifier(buildReport.summary.platformGroup);
                if (!string.IsNullOrEmpty(applicationIdentifier))
                    writer.Write($"\"applicationIdentifier\": \"{applicationIdentifier}\",");

                writer.Write($"\"canonicalFileName\": \"{GetCanonicalFileName()}\",");

#if UNITY_IOS
                writer.Write("\"ios\": {");
                writer.Write($"\"projectPath\": \"{options.locationPathName}/Unity-iPhone.xcodeproj\",");
                writer.Write("\"target\": \"Unity-iPhone\",");
                writer.Write("\"scheme\": \"Unity-iPhone\",");
                writer.Write($"\"developerTeamId\": \"{PlayerSettings.iOS.appleDeveloperTeamID}\"");
                writer.Write('}');
#endif
                writer.Write('}');
            }
        }
    }
}
