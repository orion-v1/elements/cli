﻿using System;
using System.Collections.Generic;
using UnityEditor;

namespace Orion.Elements.Cli
{
    internal class ModifyPreprocessorDirectivesCommand : UnityCommand
    {
        private HashSet<string> _directivesToAdd = new HashSet<string>();
        private HashSet<string> _directivesToRemove = new HashSet<string>();

        public override string Tag => "ModifyPreprocessorDirectives";


        public ModifyPreprocessorDirectivesCommand()
        {
            RegisterOptionHandler("define", HandleDefine);
            RegisterOptionHandler("undefine", HandleUndefine);
        }

        #region Parsing

        private void HandleDefine(CliOption option)
        {
            CliToken value = option.GetValueOrDefault(0);
            if (value != null)
                _directivesToAdd.Add(value.Text);
        }

        private void HandleUndefine(CliOption option)
        {
            CliToken value = option.GetValueOrDefault(0);
            if (value != null)
                _directivesToRemove.Add(value.Text);
        }

        #endregion

        public override void Validate()
        {
            // check build target
            AssertBuildTargetDefined();
        }

        public override int Execute()
        {
            if (_directivesToAdd.Count == 0 && _directivesToRemove.Count == 0)
                return 0;

            string defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup);
            HashSet<string> directives;

            // create set of directives and add new
            if (string.IsNullOrWhiteSpace(defines))
                directives = new HashSet<string>(_directivesToAdd);
            else
            {
                directives = new HashSet<string>(defines.Split(
                    new[] {';', ' ', ','},
                    StringSplitOptions.RemoveEmptyEntries));

                directives.UnionWith(_directivesToAdd);
            }

            // remove directives
            directives.RemoveWhere(_directivesToRemove.Contains);

            // apply directives
            defines = string.Join(";", directives);
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup, defines);
            return 0;
        }
    }
}
