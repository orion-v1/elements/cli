﻿using System;
using UnityEditor;

namespace Orion.Elements.Cli
{
    internal abstract class UnityCommand : CliCommand
    {
        public BuildTarget BuildTarget { get; private set; } = BuildTarget.NoTarget;
        public BuildTargetGroup BuildTargetGroup { get; private set; } = BuildTargetGroup.Unknown;


        public UnityCommand()
        {
            RegisterOptionHandler("buildTarget", HandleBuildTarget);
        }


        protected virtual void HandleBuildTarget(CliOption option)
        {
            CliToken value = option.GetValueOrDefault(0);
            if (value == null)
                throw new ArgumentException("No value specified for argument -buildTarget.");

            string target = value.Text.ToLowerInvariant();
            switch (target)
            {
                case "ios": BuildTarget = BuildTarget.iOS; break;
                case "android": BuildTarget = BuildTarget.Android; break;
                default:
                    throw new Exception($"Invalid value of argument -buildTarget. {target} is not supported.");
            }

            BuildTargetGroup = BuildPipeline.GetBuildTargetGroup(BuildTarget);
        }

        protected void AssertBuildTargetDefined()
        {
            // check build target
            if (BuildTarget == BuildTarget.NoTarget)
                throw new InvalidOperationException("Missing argument -buildTarget.");
        }
    }
}
