﻿using Orion.Elements.Cli;
using UnityEngine;

public class InProjectTests : ICliOptionHandler
{
    public string OptionName => "customOption2";

    public int Order => 0;

    public void Process(CliOption option)
    {
        Debug.Log($">> Invoke Process for custom option {OptionName}");
    }
}

public class InProjectTests1 : ICliOptionHandler
{
    public string OptionName => "customOption3";

    public int Order => -1;

    public void Process(CliOption option)
    {
        Debug.Log($">> Invoke Process for custom option {OptionName}");
    }
}